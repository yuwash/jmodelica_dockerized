# What is JModelica.org

JModelica.org is an extensible Modelica-based open source platform for
optimization, simulation and analysis of complex dynamic systems. The main
objective of the project is to create an industrially viable open source
platform for optimization of Modelica models, while offering a flexible
platform serving as a virtual lab for algorithm development and research. As
such, JModelica.org provides a platform for technology transfer where
industrially relevant problems can inspire new research and where state of the
art algorithms can be propagated from academia into industrial use.
JModelica.org is a result of research at the
[Department of Automatic Control, Lund University](http://www.control.lth.se/),
and is now maintained and developed by
[Modelon AB](http://www.modelon.se/)
in collaboration with academia. 

JModelica.org is distributed under the GPL v.3 license approved by the Open
Source Initiative.

[JModelica.org official homepage](https://phalconphp.com)

# How to use this image

You can use the following command to open JModelica.org iPython shell:

```sh
docker run --rm -it mclab/jmodelica
```

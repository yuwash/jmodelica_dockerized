BUILD_PKGS="g++ swig ant cmake python-dev zlib1g-dev libboost-dev jcc subversion wget patch pkg-config liblapack-dev libblas-dev openjdk-8-jdk-headless build-essential python-setuptools make clang apt-utils pkgconf libpng-dev libfreetype6-dev autoconf"
KEEP_PKGS="gcc make python-pip ipython default-jre-headless python-numpy python-scipy python-lxml python-nose python-jpype cython gfortran"
RUNTIME_PKGS="zlib1g python-matplotlib liblapack3 libblas3 python-shapely python-tk"

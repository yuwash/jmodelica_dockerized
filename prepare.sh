#! /usr/bin/env sh

. /build/config.sh

{
# ========== Create an user and environmental variables associated to it ===============
true; } \
&& adduser --disabled-password --gecos '' docker \
&& adduser docker sudo \
&& echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers \
&& mkdir -p /home/docker \
&& {
# ========== For your own Modelica libraries (mount it here) ===========================
true; } \
&& mkdir -p /home/docker/modelica


apt-get update && apt-get install -y --no-install-recommends \
	${BUILD_PKGS} ${KEEP_PKGS}

echo "export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64" >> /root/.bashrc
echo "export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64" >> /home/docker/.bashrc

pip install -U --no-cache-dir \
	pip \
&& pip install -U --no-cache-dir \
	cython

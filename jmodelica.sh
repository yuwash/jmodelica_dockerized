#! /usr/bin/env sh

NP=$(grep -c ^processor /proc/cpuinfo)

IPOPT=Ipopt-3.12.4
cd /build
wget http://www.coin-or.org/download/source/Ipopt/${IPOPT}.tgz
tar xf ${IPOPT}.tgz
for module in Blas Lapack Mumps Metis; do
  cd /build/${IPOPT}/ThirdParty/${module}
  ./get.${module}
done
cd /build/${IPOPT}
mkdir /opt/ipopt
./configure --prefix=/opt/ipopt && make && make install

mkdir /opt/jmodelica
svn co https://svn.jmodelica.org/tags/2.1 /build/jmodelica
mkdir /build/jmodelica/build
# Workaround from http://www.jmodelica.org/27850#comment-6589
cd /build/jmodelica/external
svn co https://svn.jmodelica.org/assimulo/trunk Assimulo
cd /build/jmodelica/build
../configure --prefix=/opt/jmodelica --with-ipopt=/opt/ipopt \
	&& make && make install \
	&& make install_casadi && make casadi_interface
